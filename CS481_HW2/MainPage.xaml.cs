﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        double firstNum, secondNum, result;
        string op;
        int curState = 1;
        public MainPage()
        {
            InitializeComponent();
            Clear(this, null); // want to make sure there isn't any result starting out
        }

        void Clear(System.Object sender, System.EventArgs e)
        {
            firstNum = 0; secondNum = 0;
            curState = 1;
            this.resultText.Text = "0";
        }

        void OnOperator(System.Object sender, System.EventArgs e)
        {
            curState = -2;
            Button button = (Button)sender;
            string selectedOperator = button.Text;
            op = selectedOperator;
        }
        void NumberClicked(System.Object sender, System.EventArgs e)
        {
            Button button = (Button)sender;
            string numberSelected = button.Text;

            if (this.resultText.Text == "0" || curState < 0)
            {
                this.resultText.Text = "";

                if (curState < 0)
                    curState *= -1;
            }
            this.resultText.Text += numberSelected;

            double number;
            if (double.TryParse(this.resultText.Text, out number))
            {
                this.resultText.Text = number.ToString("N0");
                if (curState == 1)
                {
                    firstNum = number;
                }
                else
                {
                    secondNum = number;
                }
            }
        }

        void Calculate(System.Object sender, System.EventArgs e)
        {
            if (curState == 2)
            {
                switch (op)
                {
                    case "/":
                        try
                        {
                            result = (firstNum / secondNum);
                        }
                        catch (DivideByZeroException)
                        {

                        }
                        break;
                    case "X":
                        result = (firstNum * secondNum);
                        break;
                    case "+":
                        result = (firstNum + secondNum);
                        break;
                    case "-":
                        result = (firstNum - secondNum);
                        break;
                    case "sqrt":
                        result = (Math.Sqrt(secondNum));

                        break;
                    case "%":
                        result = (firstNum % secondNum);
                        break;
                }
                this.resultText.Text = result.ToString();
                curState = -1;
                firstNum = result;
            }
        }
    }
}
